function generateTable()
{
    checkForTable();
    var n = document.getElementById("nInput").value;
    var m = document.getElementById("mInput").value;
    var table = document.createElement("table");
    for(let i = 0 ; i < n; i++)
    {
        let tr = document.createElement("tr");
        
        for(let j = 0; j < m; j++)
        {
            let td = document.createElement("td")
            tr.appendChild(td);
        }
        table.appendChild(tr);
    }
    document.body.appendChild(table);
}
function checkForTable()
{
    let table = document.getElementsByTagName("table")[0];
    if(table != null) table.remove(); 
}